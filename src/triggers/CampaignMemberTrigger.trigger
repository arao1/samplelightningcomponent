// 18 June  2018     Ammar Rao     Added a trigger for the campaign member object. 
trigger CampaignMemberTrigger on CampaignMember (before insert, after insert, before update, after update, before delete, after delete, after undelete) {
    if(Trigger.isAfter){
// Trigger handler for creating a new task when campaign member is added.
    CampaignMemberTask.createTask(Trigger.new);
    }
}