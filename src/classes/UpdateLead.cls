public class UpdateLead {
    @AuraEnabled public static Lead          getLead(){
        return (Lead) Database.query( ' SELECT Id, Email, Company, Status FROM Lead LIMIT 1 ' )[0];
    }
    @AuraEnabled public static Lead          saveLead(Lead lead){
        upsert lead;
        return lead;
    }   
}