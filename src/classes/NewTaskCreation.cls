// 14 June 2018     Ammar Rao     Added a trigger handler class to create a new task
public with sharing class CampaignMemberTask {
    public static void createTask(List<CampaignMember> cmembers) {
//This list creates a new task and assigns values to each variable.     	
    	List<Task> AddTask = new List<Task>();
    	
		Set<Id> cmId = new Set<Id>();
		Set<Id> cId = new Set<Id>();
		for(CampaignMember c :cmembers){
			cmId.add(c.Id);
			cId.add(c.CampaignId);
		
		Map<Id, String> campaignName = new Map<Id, String>();
    	for(Campaign cmp : [SELECT Id, Name, (SELECT Id FROM CampaignMembers WHERE Id IN :cmId) FROM Campaign WHERE Id IN :cId]){
			campaignName.put(cmp.Id, cmp.Name);
    	}
    	
    		
    		if (c.LeadId == null){
    		AddTask.add(new Task(
        	WhoId = c.ContactId,
        	WhatId = c.CampaignId,
        	Campaign_Member_Id__c = c.Id,
			Status = 'Planned',
			Subject = 'Campaign ' + campaignName.get(c.CampaignId)
	
			));
    		}
			else {
				AddTask.add(new Task(
        	WhoId = c.LeadId,
        	Campaign_Member_Id__c = c.Id,
			Status = 'Planned',
			Subject = 'Campaign '
			));
			}
}
		insert AddTask;
}

}