// 14 June 2018     Ammar Rao     Added a test class to check the functionality in NewTaskCreation class.
	@isTest
public with sharing class CampaignMemberTask_Test {
	@isTest static void contactTaskIsCreated() {
// @noOfTasks list queries all the current tasks. 
// @testAcc creates a new account record
// @testContact creates a new contact record to be linked to the campaign member.
// @newcampaign creates a new campaign record.
// @newMember creates a new campaign member record. 
// @noOfTasks2 list queries all the tasks once a new campaign member record has been added.
// system.assertEquals compares the previous and current task size. 
        List<Task> noOfTasks =[SELECT Id FROM Task];
    	Account testAcc = new Account (Name = 'Test Account2', fax='0000001', AccountNumber='0000002');
        insert testAcc;
        Contact testContact = new Contact(FirstName = 'TestContactF1', LastName = 'TestContactL2', Email = 'test111@appirio.com',accountid=testAcc.Id,LeadSource = 'community' );
        insert testContact;
        Campaign newCampaign = new Campaign(name = 'test99913');
        insert newCampaign;
        CampaignMember newMember = new CampaignMember(ContactId = testContact.id, status='Sent', campaignid = newcampaign.id);
        insert newMember;   
        List<Task> noOfTasks2 =[SELECT Id FROM Task];
        system.assertEquals(noOfTasks.size() + 1, noOfTasks2.size());
    }
    
    @isTest static void leadTaskIsCreated() {
// @noOfTasks list queries all the current tasks. 
// @testLead creates a new lead record to be linked to the campaign member.
// @newcampaign creates a new campaign record.
// @newMember creates a new campaign member record. 
// @noOfTasks2 list queries all the tasks once a new campaign member record has been added.
// system.assertEquals compares the previous and current task size. 
        List<Task> noOfTasks =[SELECT Id FROM Task];
        Lead testLead = new Lead(FirstName = 'TestLeadF', LastName = 'TestLeadL', Company = 'testcompany');
        insert testLead;
        Campaign newCampaign = new Campaign(name = 'test99913');
        insert newCampaign;
        CampaignMember newMember = new CampaignMember(LeadId = testLead.id, status='Sent', campaignid = newcampaign.id);
        insert newMember;   
        List<Task> noOfTasks2 =[SELECT Id FROM Task];
        system.assertEquals(noOfTasks.size() + 1, noOfTasks2.size());
    }
}