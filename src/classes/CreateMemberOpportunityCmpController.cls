public with sharing class CreateMemberOpportunityCmpController {

    // This list gets the record types for opportunity //
    
    public static Map<Id, String> recordtypemap {get;set;}

    @AuraEnabled
    public static List<String> getRecordTypes(){
        List<String> recordTypes = new List<String>();
        
        List<Schema.RecordTypeInfo> oppRecordTypes = Opportunity.SObjectType.getDescribe().getRecordTypeInfos();
        recordtypemap = new Map<Id, String>();
        	for(Schema.RecordTypeInfo rt: oppRecordTypes){
            if(rt.getName() != 'Master'){
                recordtypemap.put(rt.getRecordTypeId(), rt.getName());
               
            }     
        }

         return recordtypemap.values();
    }

 // This list returns the fields to display on the form //
    @AuraEnabled
    public static List<String> getOppNames(String recordTypeNames){
        List<String> oppList = new List<String>();

        oppList.add('Name');
        oppList.add('StageName');
        oppList.add('Amount');
        oppList.add('CloseDate');
        oppList.add('Type');
        
        return oppList;
    }

  //This returns the record type Ids for opportunity //
    @AuraEnabled
    public static Id getRecordTypeId(String recordTypeNames){
        Id rtId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(recordTypeNames).getRecordTypeId();
        
        return rtId;
    }

   // Check to see if opportunity exists for campaign member //
    @AuraEnabled
    public static Id checkExistingOpportunity(Id campaignMemberId){
        Id existingOpportunity = null;

        List<Opportunity> oppList = [SELECT Id, CampaignMemberId__c FROM Opportunity WHERE CampaignMemberId__c=:campaignMemberId LIMIT 1];

        if(!oppList.isEmpty()){
            existingOpportunity = oppList[0].Id;
        }
        
        return existingOpportunity;
    }

    // Get the campaign Id for the campaign member //
    @AuraEnabled
    public static Id getCampaignId(Id campaignMemberId){
        List<CampaignMember> cmembers = [SELECT Id, CampaignId FROM CampaignMember WHERE Id =:campaignMemberId];

        return cmembers[0].CampaignId;
    }


}