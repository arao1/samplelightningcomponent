({
 
	doInit: function(component, event, helper) {
        //Get Names of Record Types of the Opportunity Object//
        var action = component.get("c.getRecordTypes");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.recordTypeNames", response.getReturnValue());
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);

        //Get Campaign Id of Campaign Member//
        var action2 = component.get("c.getCampaignId");
        action2.setParams({ campaignMemberId : component.get("v.recordId") });
        action2.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.campaignId", response.getReturnValue());
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action2);
        
        
      
   },
    
      fetchListOfRecordTypes: function(component, event, helper) {
      var action = component.get("c.getRecordTypes");
      action.setCallback(this, function(response) {
         component.set("v.lstOfRecordType", response.getReturnValue());
      });
      $A.enqueueAction(action);
 
	},


        getRtValues: function(component, event, helper){
        var action2 = component.get("c.getRecordTypeId");
        action2.setParams({ recordTypeName : component.find("selectid").get("v.value") });
        action2.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.lstOfRecordType", response.getReturnValue());
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action2);

		var action = component.get("c.getOppNames");
		action.setParams({ recordTypeName : component.find("selectid").get("v.value") });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.fields", response.getReturnValue());
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
    },
    
 
    handleSubmit : function(component, event, helper) {
        event.preventDefault(); 
        
        //Find if an opportunity already contains the CampaignMemberId //
        var action = component.get("c.checkExistingOpportunity");
        action.setParams({ campaignMemberId : component.get("v.recordId") });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {

                //If an existing opportunity is found, redirect to the existing opportunity  //
                if(response.getReturnValue() != null){ 
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title: "Duplicate Found!",
                        message: "An Opportunity already exists for the campaign member. You will be redirected to the Opportunity.",
                        type: "INFORMATIONAL"
                    });
                    toastEvent.fire();  
                    var navEvt = $A.get("e.force:navigateToSObject");
                    navEvt.setParams({
                        "recordId": response.getReturnValue(),
                        "slideDevName": "detail" 
                    });
                    navEvt.fire();  

                //Otherwise, create a new opportunity
                } else {
                    var fields = event.getParam("fields");
                    fields["CampaignMemberId__c"] = component.get("v.recordId");
                    fields["CampaignId"] = component.get("v.campaignId");
                    try{
                        component.find('recordEditForm').submit(fields); 
                    } catch(err){
                        console.log(err);
                        alert(err);
                    }
                }   
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
    
    },

    handleSuccess : function(component, event, helper) {
        var payload = event.getParams().response;
        console.log('payload.id: ' + payload.id);
        
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": payload.id,
            "slideDevName": "detail" 
        });
        navEvt.fire();
    },

    handleLoad : function(component, event, helper) {

    },

    // Returns to details page
    cancelButton : function(component, event, helper) {
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": component.get("v.recordId"),
            "slideDevName": "detail" 
        });
        navEvt.fire();
    }

})