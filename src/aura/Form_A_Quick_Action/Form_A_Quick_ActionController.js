({
    init : function(component, event, helper) {
        console.log('init');
        var action2 = component.get("c.getLead");        
        action2.setCallback(this, function(response) {
            console.log(response.getReturnValue());
            component.set("v.lead", response.getReturnValue());
        });
        $A.enqueueAction(action2);
    }, 
    save : function(component, event, helper) {
        console.log('save:1');
        var action = component.get("c.saveLead");
        var lead = component.get("v.lead");
        action.setParams({"lead": lead});
        action.setCallback(this, function() {  console.log('SAVED.');  } );
        $A.enqueueAction(action);
        console.log('save:end');
    },
})